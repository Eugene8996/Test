﻿using System;
using System.IO;
using System.Text;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Net;
using System.Net.Sockets;
using System.Linq;
using System.Text.Json;


namespace client
{
    class MainClass
    {
        public static void Main(string[] args)
        {
            FileClass file = new FileClass();
            ParserClass parser = new ParserClass();
            SendDataClass send = new SendDataClass();

            send.SendData(parser.Pars(file.Readfile()));


        }
    }

    class CoordClass
    {
        public int Coord_X { get; set; }
        public int Coord_Y { get; set; }
    }

    class FileClass
    {
        private string path;
        private bool OpenFileFlag = false;
        FileStream log;

        public StreamReader Readfile()
        {
            Console.WriteLine("Enter path and file name:");

            while (OpenFileFlag == false)
            {
                try
                {
                    //path = Console.ReadLine();
                    path = "C:\\1";
                    Console.WriteLine(path);
                    log = new FileStream(path, FileMode.Open, FileAccess.Read);
                    OpenFileFlag = true;
                }
                catch (IOException)
                {
                    Console.WriteLine("Wrong path, try again");
                    OpenFileFlag = false;
                }
            }

            StreamReader reader = new StreamReader(log);
            return reader;

        }

    }

    class ParserClass
    {
        private static readonly string block_start = "###START";
        private static readonly string block_end = "###END";
        private static readonly UInt16 number_coordinates = 2;

        private int count_coordinates = 0;
        private string buf;

        public List<CoordClass> Pars(StreamReader reader)
        {
            List<CoordClass> data_to_send = new List<CoordClass>();
            string[] Separators = new string[] { " , " };
            int[] coordinates = new int[number_coordinates];
            do
            {
                buf = reader.ReadLine();
            } while (buf != block_start);

            buf = reader.ReadLine();
            while (buf != block_end)
            {
                string[] coordinates_char = buf.Split(Separators, StringSplitOptions.None);

                data_to_send.Add(new CoordClass() { Coord_X = Convert.ToInt32(coordinates_char[0]), Coord_Y = Convert.ToInt32(coordinates_char[1]) });
                count_coordinates++;
                buf = reader.ReadLine();
            }

            
            /*
            foreach (object coord in data_to_send)
            {
                int[] coord2 = coord as int[];
                Console.WriteLine(coord2[0]);
                Console.WriteLine(coord2[1]);
            }*/
            return data_to_send;
        }
    }

    class SendDataClass
    {
        // адрес и порт сервера, к которому будем подключаться
        int port = 8005; // порт сервера
        string address = "127.0.0.1"; // адрес сервера
        public void SendData(List<CoordClass> data_to_send)
        {
            try
            {
                IPEndPoint ipPoint = new IPEndPoint(IPAddress.Parse(address), port);
                Socket socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                // подключаемся к удаленному хосту
                socket.Connect(ipPoint);

                string data_to_send_char_json;

                data_to_send_char_json = JsonSerializer.Serialize<List<CoordClass>>(data_to_send);

                byte[] data = Encoding.Unicode.GetBytes(data_to_send_char_json);
                socket.Send(data);

                // получаем ответ
                data = new byte[256]; // буфер для ответа
                StringBuilder builder = new StringBuilder();
                int bytes = 0; // количество полученных байт

                do
                {
                    bytes = socket.Receive(data, data.Length, 0);
                    builder.Append(Encoding.Unicode.GetString(data, 0, bytes));
                }
                while (socket.Available > 0);
                Console.WriteLine("ответ сервера: " + builder.ToString());

                // закрываем сокет
                socket.Shutdown(SocketShutdown.Both);
                socket.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            Console.Read();
        }
    }

    

}
